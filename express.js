// импорт express
const express = require('express');
// импорт модуля "fs"
const fs = require('fs');
// импорт body-parser
const bodyParser = require('body-parser');
// инициализация express-приложения
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

let fighters = [];
//запись в переменную fighters информации с файла
fs.readFile('underlist.json', (error, data) => {
	fighters = JSON.parse(data);
});
// создание функции слушателя для GET-запросов по адресу "/user"
app.get('/user', (request, response) => {
	console.log(fighters);
	response.send("");
});
// создание функции слушателя для GET-запросов по адресу "/user/:id"
app.get('/user/:id', (request, response) => {
	let id = request.params.id;
	let fighter = fighters.find(x => x._id == id);
	//проверка есть ли герой с таким ид
	if(fighter) {
		console.log(fighter);
		response.send(`Це герой з id ${fighter._id}`);
	} else {
		response.send(`Героя з таким id немає`);
	}
});
// создание функции слушателя для POST-запросов по адресу "/user"
app.post('/user', (request, response) => {
	console.log(request.body);
	//проверка есть ли ид в запросе на добавление
	if(!request.body._id) {
		//если ид нет то сгенерировать ид
		request.body._id = +fighters[fighters.length - 1]._id + 1;
		fighters.push(request.body);
		response.send(`Героя додано з id ${request.body._id}`);
	//если ид указано, проверка не существует ли уже такой ид
	} else if(fighters.find(x => x._id == request.body._id)) {
		//если существует, генерируем новый ид
		request.body._id = +fighters[fighters.length - 1]._id + 1;
		fighters.push(request.body);
		response.send(`Героя додано з id ${request.body._id}`);
	} else {
		fighters.push(request.body);
		response.send(`Героя додано з id ${request.body._id}`);
	}
	// запись в файл
	writeInFile(fighters);
});
// создание функции слушателя для DELETE-запросов по адресу "/user/:id"
app.delete('/user/:id', (request, response) => {
	let id = request.params.id;
	let fighter = fighters.find(x => x._id == id);
	//проверка, есть ли герой с таким ид для удаления
	if(!fighter) {
		response.send(`Героя з таким id немає`);
	} else {
		let index = fighters.indexOf(fighter);
		fighters.splice(index, 1);
		response.send(`Героя з id ${fighter._id} видалено`);
	}
	// запись в файл
	writeInFile(fighters);
});
// создание функции слушателя для PUT-запросов по адресу "/user/:id"
app.put('/user/:id', (request, response) => {
	let id = request.params.id;
	let fighter = fighters.find(x => x._id == id);
	let change = request.body;
	//проверка наличия такого героя
	if(fighter) {
		//проверка существования информации для изменения
		if(change.name) {
			fighter.name = change.name;
		}
		if(change.health) {
			fighter.health = change.health;
		}
		if(change.attack) {
			fighter.attack = change.attack;
		}
		if(change.defense) {
			fighter.defense = change.defense;
		}
		if(change.source) {
			fighter.source = change.source;
		}
		response.send(`Параметри героя з id ${fighter._id} було змінено`);
	} else {
		response.send(`Героя з таким id немає`);
	}
	// запись в файл
	writeInFile(fighters);
});
//функция для записи в файл
function writeInFile(fighters) {
	let newFighters = fighters.map((obj) => JSON.stringify(obj));
	fs.writeFile('underlist.json', `[${newFighters}]`, error => {
		if(error) throw error;
	});
}
// включение сервера
app.listen(3000);